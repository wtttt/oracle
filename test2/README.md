20软工4班 王涛  202010211323
# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色wt_res_role和用户wt，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE wt_res_role;
GRANT connect,resource,CREATE VIEW TO wt_res_role;
CREATE USER wt IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER wt default TABLESPACE "USERS";
ALTER USER wt QUOTA 50M ON users;
GRANT wt_res_role TO wt;
--收回角色
REVOKE wt_res_role FROM wt;
```

> 语句“ALTER USER wt QUOTA 50M ON users;”是指授权wt用户访问users表空间，空间限额是50M。

- 第2步：新用户wt连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus wt/123@pdborcl
SQL> show user;
USER is "wt"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```

- 第3步：用户hr连接到pdborcl，查询wt授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM wt.customers;
elect * from wt.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM wt.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

> 测试一下用户hr,wt之间的表的共享，只读共享和读写共享都测试一下。
> wt用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user wt unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user wt  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色wt_res_role和用户wt。
> 新用户wt使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role wt_res_role;
drop user wt cascade;
```

## 实验总结

本次实验学习了Oracle数据库的用户及权限管理，包括创建角色、创建用户、授权、分配空间、创建表、创建视图、共享对象、概要文件设置等内容。通过实验，我掌握了以下方面的能力：

1.创建本地角色并为其授权和分配权限

2.创建用户并将其授予角色

3.测试用户所拥有的权限，包括创建表、插入数据、创建视图、查询表和视图数据等

4.了解会话权限和角色的查看方法

5.理解概要文件的作用，并设置了登录失败次数限制

此外，在实验过程中，我还了解了数据库和表空间占用分析，能够使用SQL语句查看表空间的数据库文件、磁盘占用情况和使用率等信息。最后，也学习了删除用户和角色的操作。总而言之，该实验对我熟悉Oracle数据库的用户和权限管理提供了很好的实践机会，也使我更加深入地理解了数据库中不同对象之间的关系。

## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
