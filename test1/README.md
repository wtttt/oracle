20软件工程4班 202010211323 王涛 

# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

### 实验步骤

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

```text
 v_$sesstat, v_$statname 和 v_$session
```

- 权限分配过程如下

```sql
$ sqlplus sys/123@localhost/pdborcl as sysdba
@$ORACLE_HOME/sqlplus/admin/plustrce.sql
create role plustrace;
GRANT SELECT ON v_$sesstat TO plustrace;
GRANT SELECT ON v_$statname TO plustrace;
GRANT SELECT ON v_$mystat TO plustrace;
GRANT plustrace TO dba WITH ADMIN OPTION;
GRANT plustrace TO hr;
GRANT SELECT ON v_$sql TO hr;
GRANT SELECT ON v_$sql_plan TO hr;
GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
GRANT SELECT ON v_$session TO hr;
GRANT SELECT ON v_$parameter TO hr; 
```

查询1：查询员工工资高于平均工资的员工信息以及所在部门信息

```SQL
sql语句：
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM employees
INNER JOIN departments ON employees.department_id = departments.department_id
WHERE employees.salary > (SELECT AVG(salary) FROM employees);
统计信息
----------------------------------------------------------
	 58  recursive calls
	  0  db block gets
	 35  consistent gets
	  0  physical reads
	  0  redo size
       2641  bytes sent via SQL*Net to client
	641  bytes received via SQL*Net from client
	  5  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	 50  rows processed


输出结果：

Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY"> (SELECT SUM("SALARY")/COUNT("SALARY") FROM
	      "EMPLOYEES" "EMPLOYEES"))


```

- 查询2

```SQL
sql语句：
set autotrace on
SELECT employees.employee_id, employees.first_name, employees.last_name, departments.department_name
FROM employees, departments
WHERE employees.department_id = departments.department_id
AND employees.salary > (SELECT AVG(salary) FROM employees);

统计信息
----------------------------------------------------------
	 18  recursive calls
	  2  db block gets
	 54  consistent gets
	  0  physical reads
	  0  redo size
       2641  bytes sent via SQL*Net to client
	641  bytes received via SQL*Net from client
	  5  SQL*Net roundtrips to/from client
	  1  sorts (memory)
	  0  sorts (disk)
	 50  rows processed

输出结果：
Predicate Information (identified by operation id):
---------------------------------------------------

   4 - access("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
       filter("EMPLOYEES"."DEPARTMENT_ID"="DEPARTMENTS"."DEPARTMENT_ID")
   5 - filter("EMPLOYEES"."SALARY"> (SELECT SUM("SALARY")/COUNT("SALARY") FROM
	      "EMPLOYEES" "EMPLOYEES"))

```

**查询分析：**

根据SQL语句和Autotrace统计信息可以看出，两个查询语句的结果都是查询员工工资高于平均工资的员工信息以及所在部门信息。不过第一个查询语句使用了INNER JOIN关键字进行表连接，而第二个查询语句使用了WHERE子句进行条件筛选。两个查询语句的执行计划均使用了表连接，查询的行数都是50行，但是第一个查询语句的递归调用次数和consistent gets比第二个查询语句都要多。这说明第一个查询语句比第二个查询语句更消耗资源，效率更低。因此，第二个查询语句的效率更高。

**优化建议：**

1. 使用INNER JOIN、LEFT JOIN、RIGHT JOIN等关键字进行表连接，而不是使用WHERE子句进行条件筛选。
2. 避免在SELECT子句中使用通配符“*”，而是尽可能明确地列出需要查询的字段。
3. 尽量避免使用子查询，可以通过连接或者临时表的方式来实现相同的功能。
4. 在需要进行大量数据操作时，尽量使用批量操作，如INSERT INTO ... SELECT ...，而不是一条一条地操作。
5. 避免在查询中使用函数，特别是聚合函数，因为它们会增加查询的复杂度和计算量。

**实验总结：**

本次实验主要学习了如何对Oracle数据库中的表进行查询与分析，同时了解了SQL执行计划的重要作用以及如何通过优化指导工具对SQL语句进行优化。

在实验中，我们使用了HR人力资源管理系统中的表进行了查询与分析，通过对SQL语句的执行计划进行分析，可以更加深入地了解SQL语句的执行过程，并且发现其中的性能瓶颈，从而优化查询语句以提升查询效率。

在自己设计的查询语句中，我设计了两个较为复杂的SQL语句，通过分析它们的执行计划，判断哪个SQL语句是最优的。实验结果显示，优化后的SQL语句在执行效率上比未优化的SQL语句有了明显的提升。

## 实验注意事项

- 完成时间：2023-3-21，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test1目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
