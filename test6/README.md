王涛  202010211323  20软工4班

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。



### 1.创建表空间
- wt_space1
```sql
Create Tablespace wt_space1
datafile
'/home/oracle/app/oracle/oradata/orcl/wt_space1_1.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wt_space1_2.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```
-wt_ space2
```sql
Create Tablespace wt_space2
datafile
'/home/oracle/app/oracle/oradata/orcl/wt_space2_1.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wt_space2_2.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;
```

![](../test6/pict1.JPG)

- product（商品表）：id（产品ID）name（产品名称）description（产品描述）price（价格）stock（库存）category_id（类别ID）
- customer（顾客表）id（顾客ID）name（顾客姓名）email（电子邮件）phone_number（电话号码）address（地址）
- order（订单表）id（订单ID）customer_id（顾客ID）order_date（下单日期）status（订单状态）
- shipment（发货表）id（发货ID）order_id（订单ID）tracking_number（快递单号）carrier（快递公司）ship_date（发货日期）


### 2.创建表
- product（商品表）
```sql
CREATE TABLE wt_product (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    name VARCHAR2(255) NOT NULL,
    description CLOB,
    price NUMBER(10, 2) NOT NULL,
    stock NUMBER NOT NULL,
    category_id NUMBER,
    PRIMARY KEY (id)
);
```
- customer（顾客表）
```sql
CREATE TABLE wt_customer (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    name VARCHAR2(255) NOT NULL,
    email VARCHAR2(255) NOT NULL UNIQUE,
    phone_number VARCHAR2(20),
    address CLOB,
    PRIMARY KEY (id)
);
```

- order（订单表）
```sql
CREATE TABLE wt_order (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    customer_id NUMBER NOT NULL,
    order_date TIMESTAMP NOT NULL,
    status VARCHAR2(50) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES wt_customer(id)
);
```
- shipment（发货表）
```sql
CREATE TABLE wt_shipment (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    order_id NUMBER NOT NULL,
    tracking_number VARCHAR2(50) NOT NULL,
    carrier VARCHAR2(100) NOT NULL,
    ship_date TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES wt_order(id)
);
```
![](../test6/pict2.JPG)


### 插入数据
- 商品表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_product (name, price, stock)
    VALUES ( 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 500), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;
```
![](../test6/pict3.JPG)
- 顾客表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_customer (name, email, phone_number,address)
    VALUES ( 'Customer ' || i,  i || 'qq.com', '172082' || i,'address'|| i);
  END LOOP;
  COMMIT;
END;

```
![](../test6/pict4.JPG)

- 订单表
```sql
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_order (customer_id, order_date, status)
    VALUES (ROUND(DBMS_RANDOM.VALUE(1, 50000), 0),  TRUNC(SYSDATE - DBMS_RANDOM.VALUE(1, 365)), 'finish');
  END LOOP;
  COMMIT;
END;

```
![](../test6/pict5.JPG)

- 发货表
```sql
#快递编号设计  不重复50000个编号
#创建一个计数器表，用于存储已经生成的随机数数量和当前最大值
CREATE TABLE counter_table (
  count NUMBER,
  max_value NUMBER
);
INSERT INTO counter_table VALUES (0, 9999999999); -- 10位数的最大值为9999999999

#使用循环和条件语句来生成和插入随机数字，直到生成50000个不重复随机数字为止
DECLARE
  random_number NUMBER(10);
  my_count NUMBER(10);
  my_max_value NUMBER(10);
BEGIN
  SELECT COUNT(*) INTO my_count FROM counter_table;
  WHILE my_count < 50000 LOOP
    SELECT MAX(max_value) INTO my_max_value FROM counter_table;
    SELECT TRUNC(DBMS_RANDOM.VALUE(1000000000, my_max_value)) INTO random_number FROM DUAL;
    SELECT COUNT(*) INTO my_count FROM counter_table WHERE count = random_number;
    IF my_count = 0 THEN
      INSERT INTO counter_table (count) VALUES (random_number);
      UPDATE counter_table SET count = count + 1 WHERE count <= 50000; -- 更新计数器
    END IF;
    SELECT COUNT(*) INTO my_count FROM counter_table; -- 重新计算总行数
  END LOOP;
END;

#插入数据
DECLARE
  t_tracking NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    SELECT MAX(count) INTO t_tracking FROM counter_table; -- 将SELECT语句移到循环内部
    INSERT INTO wt_shipment (order_id, tracking_number, carrier, ship_date)
    VALUES (ROUND(DBMS_RANDOM.VALUE(1, 50000), 0), t_tracking, 'shunfeng', TRUNC(SYSDATE - DBMS_RANDOM.VALUE(1, 365)));

    UPDATE counter_table SET count = count + 1; -- 更新计数器
  END LOOP;
  COMMIT;
END;

```
![](../test6/pict6.JPG)

### 设计权限及用户分配方案
```sql
 #创建管理员用户授予DBA角色。DBA角色具有对数据库的完全控制权限，包括创建和管理用户、表空间等
CREATE USER wt_admin IDENTIFIED BY Wt123 DEFAULT TABLESPACE wt_space1 TEMPORARY TABLESPACE temp;
GRANT DBA TO wt_admin;

 #创建普通用户 允许用户创建会话、创建表和序列、以及在其默认表空间中插入、更新和删除数据
CREATE USER normal_user IDENTIFIED BY Normal123 DEFAULT TABLESPACE wt_space2 TEMPORARY TABLESPACE temp;
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE TO normal_user;
#限制用户在表空间中的存储配额
ALTER USER normal_user QUOTA 300M ON users;
```
![](../test6/pict7.JPG)

### 程序包
```sql
CREATE OR REPLACE PACKAGE wt_package AS
  -- 存储过程：创建新产品
  PROCEDURE create_product(
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER);

  -- 存储过程：更新产品信息
  PROCEDURE update_product(
    p_id IN NUMBER,
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER);

  -- 存储过程：删除产品
  PROCEDURE delete_product(p_id IN NUMBER);

  -- 存储过程：创建新客户
  PROCEDURE create_customer(
    p_name IN VARCHAR2,
    p_email IN VARCHAR2,
    p_phone_number IN VARCHAR2,
    p_address IN CLOB);

  -- 函数：获取客户订单数量
  FUNCTION get_order_count(p_customer_id IN NUMBER) RETURN NUMBER;

  -- 存储过程：创建新订单
  PROCEDURE create_order(p_customer_id IN NUMBER);

  -- 存储过程：更新订单状态
  PROCEDURE update_order_status(p_order_id IN NUMBER, p_status IN VARCHAR2);

  -- 存储过程：创建新物流
  PROCEDURE create_shipment(
    p_order_id IN NUMBER,
    p_tracking_number IN VARCHAR2,
    p_carrier IN VARCHAR2,
    p_ship_date IN TIMESTAMP);
END wt_package;
/


#创建包实体
CREATE OR REPLACE PACKAGE BODY wt_package AS

  PROCEDURE create_product(
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER) IS
  BEGIN
    INSERT INTO wt_product (name, description, price, stock, category_id)
    VALUES (p_name, p_description, p_price, p_stock, p_category_id);
  END create_product;

  PROCEDURE update_product(
    p_id IN NUMBER,
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER) IS
  BEGIN
    UPDATE wt_product
    SET name = p_name,
        description = p_description,
        price = p_price,
        stock = p_stock,
        category_id = p_category_id
    WHERE id = p_id;
  END update_product;

  PROCEDURE delete_product(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM wt_product WHERE id = p_id;
  END delete_product;

  PROCEDURE create_customer(
    p_name IN VARCHAR2,
    p_email IN VARCHAR2,
    p_phone_number IN VARCHAR2,
    p_address IN CLOB) IS
  BEGIN
    INSERT INTO wt_customer (name, email, phone_number, address)
    VALUES (p_name, p_email, p_phone_number, p_address);
  END create_customer;

  FUNCTION get_order_count(p_customer_id IN NUMBER) RETURN NUMBER IS
    v_order_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_order_count
    FROM wt_order
    WHERE customer_id = p_customer_id;

    RETURN v_order_count;
  END get_order_count;

  PROCEDURE create_order(p_customer_id IN NUMBER) IS
  BEGIN
    INSERT INTO wt_order (customer_id, order_date)
    VALUES (p_customer_id, SYSDATE);
  END create_order;

  PROCEDURE update_order_status(p_order_id IN NUMBER, p_status IN VARCHAR2) IS
  BEGIN
    UPDATE wt_order
    SET status = p_status
    WHERE id = p_order_id;
  END update_order_status;

  PROCEDURE create_shipment(
    p_order_id IN NUMBER,
    p_tracking_number IN VARCHAR2,
    p_carrier IN VARCHAR2,
    p_ship_date IN TIMESTAMP) IS
  BEGIN
    INSERT INTO wt_shipment (order_id, tracking_number, carrier, ship_date)
    VALUES (p_order_id, p_tracking_number, p_carrier, p_ship_date);
  END create_shipment;

END wt_package;
/
```
![](../test6/pict8.JPG)


###  测试
```sql
DECLARE
BEGIN
  wt_package.create_product(
    p_name => '新产品',
    p_description => '这是一个新产品的描述',
    p_price => 100,
    p_stock => 50,
    p_category_id => 1
  );
END;
/

select * from wt_product where id=50001
```

![](../test6/pict9.JPG)

## 备份方案

1. 创建备份脚本：首先，在一个可访问的目录（如/home/oracle/scripts/）中创建两个脚本文件：一个用于全备份，另一个用于增量备份。例如，创建名为full_backup.sh和incremental_backup.sh的脚本文件。

2. 编写全备份脚本: 编辑full_backup.sh并添加以下内容：
```sql
bash
#!/bin/bash
ORACLE_SID=your_oracle_sid
ORACLE_HOME=/u01/app/oracle/product/12.2.0/dbhome_1
export ORACLE_SID ORACLE_HOME
PATH=$ORACLE_HOME/bin:$PATH
export PATH

rman target / << EOF
run {
    CONFIGURE CONTROLFILE AUTOBACKUP ON;
    CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup_location/%F';
    CONFIGURE DEVICE TYPE DISK PARALLELISM 2 BACKUP TYPE TO COMPRESSED BACKUPSET;

    BACKUP AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
}
exit;
EOF
修改 your_oracle_sid 为我的Oracle SID，并根据实际情况更新 ORACLE_HOME 变量的值。将 /backup_location/ 替换为实际的备份目录。

编写增量备份脚本: 编辑incremental_backup.sh并添加以下内容：

bash
#!/bin/bash

ORACLE_SID=your_oracle_sid
ORACLE_HOME=/u01/app/oracle/product/12.2.0/dbhome_1
export ORACLE_SID ORACLE_HOME
PATH=$ORACLE_HOME/bin:$PATH
export PATH

rman target / << EOF
run {
    CONFIGURE CONTROLFILE AUTOBACKUP ON;
    CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '/backup_location/%F';
    CONFIGURE DEVICE TYPE DISK PARALLELISM 2 BACKUP TYPE TO COMPRESSED BACKUPSET;

    BACKUP INCREMENTAL LEVEL 1 CUMULATIVE AS COMPRESSED BACKUPSET DATABASE PLUS ARCHIVELOG;
}
exit;
EOF
修改 your_oracle_sid 为我的Oracle SID，并根据实际情况更新 ORACLE_HOME 变量的值。将 /backup_location/ 替换为实际的备份目录。
```
3. 设置脚本权限: 确保脚本文件具有执行权限。运行以下命令以设置适当的权限：

bash
chmod +x /home/oracle/scripts/full_backup.sh
chmod +x /home/oracle/scripts/incremental_backup.sh
配置定时任务: 使用crontab配置定时任务以按计划自动运行全备份和增量备份脚本。运行crontab -e来编辑定时任务，然后添加以下两行（假设脚本位于/home/oracle/scripts/）：

00 01 * * 0 /home/oracle/scripts/full_backup.sh > /home/oracle/scripts/full_backup.log 2>&1
00 01 * * 1-6 /home/oracle/scripts/incremental_backup.sh > /home/oracle/scripts/incremental_backup.log 2>&1
上述配置将在每周日凌晨1点执行全备份，并在其他天（周一至周六）的凌晨1点执行增量备份。根据实际需求调整时间。

4. 监控备份: 定期检查/home/oracle/scripts/full_backup.log和/home/oracle/scripts/incremental_backup.log以确保备份成功完成。还应定期验证备份以确保它们在需要时可以正确恢复。

5. 测试恢复过程: 周期性地（例如，每季度一次）测试恢复过程以确保备份有效，并熟悉恢复过程。这可能包括从备份中恢复整个数据库或从备份中恢复特定表。

6. 验证备份: 使用RMAN的VALIDATE命令来验证备份。此命令将检查备份是否可以用于恢复，但实际上不会执行恢复。以下是一个简单的示例：

rman target / << EOF
RESTORE DATABASE VALIDATE;
exit;
EOF
还可以验证特定的数据文件和控制文件。验证完成后，检查输出以确保没有错误。

7. 维护备份目录: 定期清理过时的备份以释放磁盘空间。可以使用RMAN的DELETE命令来删除过时的备份。例如，以下命令将删除早于7天的备份：

rman target / << EOF
DELETE NOPROMPT OBSOLETE RECOVERY WINDOW OF 7 DAYS;
exit;
EOF
8. 监控磁盘空间: 监控备份存储位置的磁盘空间，以防止由于空间不足而导致备份失败。可以使用操作系统工具（如Linux中的df命令）来检查磁盘空间。

9. 安全性考虑: 确保备份文件受到适当的保护，以防止未经授权的访问。考虑将备份加密以提高安全性
## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|
