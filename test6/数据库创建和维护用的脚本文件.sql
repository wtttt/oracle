Create Tablespace wt_space1
datafile
'/home/oracle/app/oracle/oradata/orcl/wt_space1_1.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wt_space1_2.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

Create Tablespace wt_space2
datafile
'/home/oracle/app/oracle/oradata/orcl/wt_space2_1.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED,
'/home/oracle/app/oracle/oradata/orcl/wt_space2_2.dbf'
  SIZE 500M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;


CREATE TABLE wt_product (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    name VARCHAR2(255) NOT NULL,
    description CLOB,
    price NUMBER(10, 2) NOT NULL,
    stock NUMBER NOT NULL,
    category_id NUMBER,
    PRIMARY KEY (id)
);


CREATE TABLE wt_customer (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    name VARCHAR2(255) NOT NULL,
    email VARCHAR2(255) NOT NULL UNIQUE,
    phone_number VARCHAR2(20),
    address CLOB,
    PRIMARY KEY (id)
);

CREATE TABLE wt_order (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    customer_id NUMBER NOT NULL,
    order_date TIMESTAMP NOT NULL,
    status VARCHAR2(50) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (customer_id) REFERENCES wt_customer(id)
);


CREATE TABLE wt_shipment (
    id NUMBER GENERATED ALWAYS AS IDENTITY,
    order_id NUMBER NOT NULL,
    tracking_number VARCHAR2(50) NOT NULL,
    carrier VARCHAR2(100) NOT NULL,
    ship_date TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES wt_order(id)
);

BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_product (name, price, stock)
    VALUES ( 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(1, 500), 2), ROUND(DBMS_RANDOM.VALUE(1, 100), 0));
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_customer (name, email, phone_number,address)
    VALUES ( 'Customer ' || i,  i || 'qq.com', '172082' || i,'address'|| i);
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_order (customer_id, order_date, status)
    VALUES (ROUND(DBMS_RANDOM.VALUE(1, 50000), 0),  TRUNC(SYSDATE - DBMS_RANDOM.VALUE(1, 365)), 'finish');
  END LOOP;
  COMMIT;
END;

BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO wt_shipment (order_id,tracking_number ,order_date, status)
    VALUES (ROUND(DBMS_RANDOM.VALUE(1, 50000), 0), , 'finish');
  END LOOP;
  COMMIT;
END;

DECLARE
  random_number NUMBER(10);
  temp_table_count NUMBER;
  temp_table_exists NUMBER(1);
BEGIN
  WHILE temp_table_count < 50000 LOOP
    SELECT COUNT(*) INTO temp_table_count FROM temp_table;
    SELECT TRUNC(DBMS_RANDOM.VALUE(1000000000, 9999999999)) INTO random_number FROM DUAL;
    SELECT COUNT(*) INTO temp_table_exists FROM temp_table WHERE number_column = random_number;
    IF temp_table_exists = 0 THEN
      INSERT INTO temp_table (number_column) VALUES (random_number);
    END IF;
  END LOOP;
END;

INSERT INTO target_table (number_column)
SELECT number_column FROM temp_table;


DECLARE
  t_tracking NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
  SELECT MAX(count) INTO t_tracking FROM counter_table;
    INSERT INTO wt_shipment (order_id, tracking_number, carrier, ship_date)
    VALUES (ROUND(DBMS_RANDOM.VALUE(1, 50000), 0), t_tracking, 'shunfeng', TRUNC(SYSDATE - DBMS_RANDOM.VALUE(1, 365)));
  END LOOP;
  COMMIT;
END;


CREATE TABLE counter_table (
  count NUMBER,
  max_value NUMBER
);
INSERT INTO counter_table VALUES (0, 9999999999); -- 10位数的最大值为9999999999

DECLARE
  random_number NUMBER(10);
  my_count NUMBER(10);
  my_max_value NUMBER(10);
BEGIN
  SELECT COUNT(*) INTO my_count FROM counter_table;
  WHILE my_count < 50000 LOOP
    SELECT MAX(max_value) INTO my_max_value FROM counter_table;
    SELECT TRUNC(DBMS_RANDOM.VALUE(1000000000, my_max_value)) INTO random_number FROM DUAL;
    SELECT COUNT(*) INTO my_count FROM counter_table WHERE count = random_number;
    IF my_count = 0 THEN
      INSERT INTO counter_table (count) VALUES (random_number);
      UPDATE counter_table SET count = count + 1 WHERE count <= 50000; -- 更新计数器
    END IF;
    SELECT COUNT(*) INTO my_count FROM counter_table; -- 重新计算总行数
  END LOOP;
END;


CREATE USER wt_admin IDENTIFIED BY Wt123 DEFAULT TABLESPACE wt_space1 TEMPORARY TABLESPACE temp;
GRANT DBA TO wt_admin;

CREATE USER normal_user IDENTIFIED BY Normal123 DEFAULT TABLESPACE wt_space2 TEMPORARY TABLESPACE temp;
GRANT CREATE SESSION, CREATE TABLE, CREATE SEQUENCE TO normal_user;
ALTER USER normal_user QUOTA 300M ON users;

CREATE OR REPLACE PACKAGE wt_package AS
  -- 存储过程：创建新产品
  PROCEDURE create_product(
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER);

  -- 存储过程：更新产品信息
  PROCEDURE update_product(
    p_id IN NUMBER,
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER);

  -- 存储过程：删除产品
  PROCEDURE delete_product(p_id IN NUMBER);

  -- 存储过程：创建新客户
  PROCEDURE create_customer(
    p_name IN VARCHAR2,
    p_email IN VARCHAR2,
    p_phone_number IN VARCHAR2,
    p_address IN CLOB);

  -- 函数：获取客户订单数量
  FUNCTION get_order_count(p_customer_id IN NUMBER) RETURN NUMBER;

  -- 存储过程：创建新订单
  PROCEDURE create_order(p_customer_id IN NUMBER);

  -- 存储过程：更新订单状态
  PROCEDURE update_order_status(p_order_id IN NUMBER, p_status IN VARCHAR2);

  -- 存储过程：创建新物流
  PROCEDURE create_shipment(
    p_order_id IN NUMBER,
    p_tracking_number IN VARCHAR2,
    p_carrier IN VARCHAR2,
    p_ship_date IN TIMESTAMP);
END wt_package;
/


CREATE OR REPLACE PACKAGE BODY wt_package AS

  PROCEDURE create_product(
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER) IS
  BEGIN
    INSERT INTO wt_product (name, description, price, stock, category_id)
    VALUES (p_name, p_description, p_price, p_stock, p_category_id);
  END create_product;

  PROCEDURE update_product(
    p_id IN NUMBER,
    p_name IN VARCHAR2,
    p_description IN CLOB,
    p_price IN NUMBER,
    p_stock IN NUMBER,
    p_category_id IN NUMBER) IS
  BEGIN
    UPDATE wt_product
    SET name = p_name,
        description = p_description,
        price = p_price,
        stock = p_stock,
        category_id = p_category_id
    WHERE id = p_id;
  END update_product;

  PROCEDURE delete_product(p_id IN NUMBER) IS
  BEGIN
    DELETE FROM wt_product WHERE id = p_id;
  END delete_product;

  PROCEDURE create_customer(
    p_name IN VARCHAR2,
    p_email IN VARCHAR2,
    p_phone_number IN VARCHAR2,
    p_address IN CLOB) IS
  BEGIN
    INSERT INTO wt_customer (name, email, phone_number, address)
    VALUES (p_name, p_email, p_phone_number, p_address);
  END create_customer;

  FUNCTION get_order_count(p_customer_id IN NUMBER) RETURN NUMBER IS
    v_order_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_order_count
    FROM wt_order
    WHERE customer_id = p_customer_id;

    RETURN v_order_count;
  END get_order_count;

  PROCEDURE create_order(p_customer_id IN NUMBER) IS
  BEGIN
    INSERT INTO wt_order (customer_id, order_date)
    VALUES (p_customer_id, SYSDATE);
  END create_order;

  PROCEDURE update_order_status(p_order_id IN NUMBER, p_status IN VARCHAR2) IS
  BEGIN
    UPDATE wt_order
    SET status = p_status
    WHERE id = p_order_id;
  END update_order_status;

  PROCEDURE create_shipment(
    p_order_id IN NUMBER,
    p_tracking_number IN VARCHAR2,
    p_carrier IN VARCHAR2,
    p_ship_date IN TIMESTAMP) IS
  BEGIN
    INSERT INTO wt_shipment (order_id, tracking_number, carrier, ship_date)
    VALUES (p_order_id, p_tracking_number, p_carrier, p_ship_date);
  END create_shipment;

END wt_package;
/

DECLARE
BEGIN
  wt_package.create_product(
    p_name => '新产品',
    p_description => '这是一个新产品的描述',
    p_price => 100,
    p_stock => 50,
    p_category_id => 1
  );
END;
/

select * from wt_product where id=50001
