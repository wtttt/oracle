学号：202010211323 姓名：王涛  班级：4班

# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 代码

```sql
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    LEFTSPACE:=' ';
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

![](../test5/pict1.png)

## 测试

```sh

函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

输出：
DEPARTMENT_ID DEPARTMENT_NAME                SALARY_TOTAL
------------- ------------------------------ ------------
           10 Administration                 4848
           20 Marketing                      19896
           30 Purchasing                     27588
           40 Human Resources                6948
           50 Shipping                       176560

过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/

输出：
101 Neena
    108 Nancy
        109 Daniel
        110 John
        111 Ismael
        112 Jose Manuel
        113 Luis
    200 Jennifer
    203 Susan
    204 Hermann
    205 Shelley
        206 William
```

## 运行结果图

![](../test5/pict2.png)
![](../test5/pict3.png)


## 实验总结

在这次实验中，主要学习了PL/SQL编程语言的结构和常用编程元素，如变量、常量、包、函数和过程。在实验过程中，我掌握了声明和使用变量与常量的方法，同时也学会了如何创建包、函数和过程。本次实验让我深入了解了以下几个概念：包是PL/SQL代码中的逻辑组件，由规范和体构成，能够封装特定的逻辑功能。函数则用于执行特定操作并将结果返回给调用程序，可以接受多个输入参数，并具有不可变性。过程与函数类似，但其不返回值，而是执行一系列操作、动作或任务。此外，实验还涉及到了变量和常量的使用。变量通常用于存储程序执行期间可能发生变化的数据值，而常量则在程序中被定义为固定值，且其值不可更改。我们还研究了游标，它是一种用于遍历查询结果集的数据库对象，用于处理返回多行数据的SQL查询结果集。通过本次实验，我对PL/SQL语言和编程有了更深入的了解。

